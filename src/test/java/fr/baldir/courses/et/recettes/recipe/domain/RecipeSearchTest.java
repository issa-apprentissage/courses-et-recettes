package fr.baldir.courses.et.recettes.recipe.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

// Pour démo des tests sociables. En vrai on utilisera plutôt Cucumber
// Les tests unitaires seraient plutôt des tests "Solitaires"
public class RecipeSearchTest {

    private RecipeSearchTestDouble recipeSearch;

    @BeforeEach
    void setup() {
        this.recipeSearch = RecipeSearchTestDouble.inMemory();
    }

    @Test
    public void base_contenant_ingredient_precis() {

        //Given
        recipeSearch.withARecipeThatContainsIngredient("Purée", "patate");
        recipeSearch.withARecipeThatContainsIngredient("Frites", "patate");

        // When
        recipeSearch.byIngredientName("patate");

        // Then
        recipeSearch.verifyRecipesSearchedByIngredientNameAre("Purée", "Frites");
    }

    @Test
    public void recette_contenant_riz() {
        //Given
        recipeSearch.withARecipeThatContainsIngredient("riz cantonais", "riz");
        recipeSearch.withARecipeThatContainsIngredient("shushi", "riz");

        // When
        recipeSearch.byIngredientName("riz");

        // Then
        recipeSearch.verifyRecipesSearchedByIngredientNameAre("riz cantonais", "shushi");
    }

    @ParameterizedTest
    @CsvSource({
            "riz cantonnais   , shushi       , riz",
            "frites           , purée        , patate",
            "salade de tomates, sauce tomate , tomate",
    })
    public void recettes_contenant_le_meme_ingredient(String recipe1,String recipe2,String ingredient) {
        //Given
        recipeSearch.withARecipeThatContainsIngredient(recipe1, ingredient);
        recipeSearch.withARecipeThatContainsIngredient(recipe2, ingredient);

        // When
        recipeSearch.byIngredientName(ingredient);

        // Then
        recipeSearch.verifyRecipesSearchedByIngredientNameAre(recipe1,recipe2);
    }
}
