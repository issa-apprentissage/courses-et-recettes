package fr.baldir.courses.et.recettes.recipe.domain;

import fr.baldir.courses.et.recettes.docs.TestDoubleComponent;
import fr.baldir.courses.et.recettes.recipe.domain.search.AddRecipeToDataSet;
import fr.baldir.courses.et.recettes.recipe.domain.secondary.RecipeRepository;
import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.inmemory.InMemoryRecipeRepository;
import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.inmemory.MutableMapAddRecipeToDataSet;
import org.assertj.core.api.Assertions;

import java.util.*;
import java.util.function.BiConsumer;

@TestDoubleComponent
public class RecipeSearchTestDouble extends RecipeSearch {
    /**
     * Fonction permettant d'ajouter une recette au jeu de donnée initial.
     */
    private final BiConsumer<String, Recipe> addRecipeToDataSet;
    private List<Recipe> recipesSearchedByIngredientName;

    // -------- Setup / Arrange / Given

    public RecipeSearchTestDouble(AddRecipeToDataSet addRecipeToDataSet, RecipeRepository recipeRepository) {
        super(recipeRepository);
        this.addRecipeToDataSet = addRecipeToDataSet;
    }

    public static RecipeSearchTestDouble inMemory() {
        Map<String, List<Recipe>> recipesDataStore = new HashMap<>();
        return new RecipeSearchTestDouble(
                new MutableMapAddRecipeToDataSet(recipesDataStore),
                new InMemoryRecipeRepository(recipesDataStore));
    }

    /**
     * Ajoute une recette dans le jeu de données initial.
     *
     * @param recipeName
     * @param ingredientName
     */
    public void withARecipeThatContainsIngredient(String recipeName, String ingredientName) {
        this.addRecipeToDataSet.accept(ingredientName, new Recipe(recipeName, UUID.randomUUID()));
    }

    // -------- Act / When

    @Override
    public List<Recipe> byIngredientName(String ingredientName) {
        this.recipesSearchedByIngredientName = super.byIngredientName(ingredientName);
        return recipesSearchedByIngredientName;
    }

    @Override
    public List<Recipe> byInAllgredientName(Collection<String> nomIngredient) {
        this.recipesSearchedByIngredientName = super.byInAllgredientName(nomIngredient);
        return recipesSearchedByIngredientName;
    }

    // -------- Assert / Then

    public void verifyRecipesSearchedByIngredientNameAre(String... recettes) {
        Assertions.assertThat(recipesSearchedByIngredientName)
                .describedAs("Recipes found when searching by ingredient name")
                .map(Recipe::render)
                .containsExactly(recettes);
    }

    public void verifyRecipesSearchedByIngredientNameContains(String recette) {
        Assertions.assertThat(recipesSearchedByIngredientName)
                .describedAs("Recipes found when searching by ingredient name")
                .map(Recipe::render)
                .contains(recette);
    }

    public void verifyRecipesSearchByIngredientIsEmpty() {
        Assertions.assertThat(recipesSearchedByIngredientName)
                .describedAs("No recipe found when searching by ingredient name")
                .isEmpty();
    }

}

