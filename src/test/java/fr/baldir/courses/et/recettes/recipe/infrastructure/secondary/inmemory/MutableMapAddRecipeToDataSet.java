package fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.inmemory;

import fr.baldir.courses.et.recettes.recipe.domain.Recipe;
import fr.baldir.courses.et.recettes.recipe.domain.search.AddRecipeToDataSet;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

/**
 * Add recipe in test dataset which is a provided mutable map.
 */
public class MutableMapAddRecipeToDataSet implements AddRecipeToDataSet {

    private final Map<String, List<Recipe>> recipesDataStore;

    public MutableMapAddRecipeToDataSet(Map<String, List<Recipe>> recipesDataStore) {
        this.recipesDataStore = recipesDataStore;
    }

    @Override
    public void accept(String ingredientName, Recipe recipe) {
        recipesDataStore.merge(ingredientName,
                List.of(recipe),
                (existingRecipes, additionalRecipes) ->
                        Stream.concat(existingRecipes.stream(), additionalRecipes.stream())
                                .toList());
    }

    @NotNull
    @Override
    public BiConsumer<String, Recipe> andThen(@NotNull BiConsumer<? super String, ? super Recipe> after) {
        return AddRecipeToDataSet.super.andThen(after);
    }
}
