package fr.baldir.courses.et.recettes.application;

import fr.baldir.courses.et.recettes.recipe.domain.RecipeSearch;
import fr.baldir.courses.et.recettes.recipe.domain.Recipe;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Teste que la configration de Spring fonctionne en mode "in-memory"
 */
@SpringBootTest
@ActiveProfiles({"dbInMem"})
public class InMemoryApplicationSmokeIT {

    @Autowired
    RecipeSearch recipeSearch;

    @Test
    public void testSmoke() {
        var results = recipeSearch.byIngredientName("patate");

        // In memory recipe repository configuration comes with hardcoded initial data
        assertThat(results).map(Recipe::name).containsExactly("Purée", "Frites");
    }
}
