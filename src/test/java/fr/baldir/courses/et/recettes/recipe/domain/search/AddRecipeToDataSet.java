package fr.baldir.courses.et.recettes.recipe.domain.search;

import fr.baldir.courses.et.recettes.recipe.domain.Recipe;

import java.util.function.BiConsumer;

/**
 *
 * Fonction représentée sous forme d'une interface avec une seule méthode ({@link FunctionalInterface}).
 * Permet de passer des fonctions sous forme de variable.
 * @see <a href="https://refactoring.guru/fr/design-patterns/strategy">Refactoring Guru — Stratégie</a>
 */
@FunctionalInterface
public interface AddRecipeToDataSet extends BiConsumer<String, Recipe> {

    void accept(String ingredientName, Recipe recipe);
}
