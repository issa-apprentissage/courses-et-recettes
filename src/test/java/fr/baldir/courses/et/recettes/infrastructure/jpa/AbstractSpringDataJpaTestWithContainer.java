package fr.baldir.courses.et.recettes.infrastructure.jpa;

import org.junit.jupiter.api.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Base de tests s’appuyant sur les testcontainers.
 * Les classes de test qui héritent de celle-ci peuvent s’appuyer sur une base de donnée
 * PostgreSQL qui est créée dans un conteneur et détruite une fois que l’ensemble des
 * tests qui s’appuient dessus sont terminés.
 * <p>
 * <p>
 * L’annotation {@link DataJpaTest} ne scanne et n’injecte que le contexte spécifique à la persistence JPA.
 * <p>
 * De ce fait, ce sont des tests qui peuvent être plutôt lents.
 * Privilégier pour tester l’intégration de la couche de persistence avec PostgreSQL.
 * <p>
 * Déconseillé pour des tests unitaires dont on attend une exécution très rapide.
 * <p>
 * Ces tests peuvent être exécutés via maven à l’aide de la commande :
 * <pre><code>
 * mvn integration-test
 * </code></pre>
 *
 * @see <a href="https://www.testcontainers.org/">Testcontainers</a>
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Testcontainers
@ActiveProfiles({"db", "postgres-testcontainer"})
@Tag("slow")
@Tag("database")
public abstract class AbstractSpringDataJpaTestWithContainer {

    @Autowired
    TestEntityManager testEntityManager;

    @Container
    private static final PostgreSQLContainer<?> postgres =
            new PostgreSQLContainer<>("postgres:15.1")
                    .withDatabaseName("postgres")
                    .withUsername("postgres")
                    .withPassword("cnr_password");

}