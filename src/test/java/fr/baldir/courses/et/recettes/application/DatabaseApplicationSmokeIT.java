package fr.baldir.courses.et.recettes.application;

import fr.baldir.courses.et.recettes.recipe.domain.Recipe;
import fr.baldir.courses.et.recettes.recipe.domain.RecipeSearch;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Teste que la configration de Spring fonctionne avec une base de données
 */
@SpringBootTest(properties = {"spring.sql.init.mode=always"})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Testcontainers
@ActiveProfiles({"db", "postgres-testcontainer"})
@Tag("slow")
@Tag("database")
public class DatabaseApplicationSmokeIT {

    @Autowired
    RecipeSearch recipeSearch;

    @Container
    private final static PostgreSQLContainer<?> postgres =
            new PostgreSQLContainer<>("postgres:15.1")
                    .withDatabaseName("postgres")
                    .withUsername("postgres")
                    .withPassword("cnr_password");

    @Test
    public void testSmoke() {

        var results = recipeSearch.byIngredientName("patate");

        // Tests that sql initialization script is run properly
        assertThat(results)
                .map(Recipe::render)
                .containsExactly("purée", "frites");
    }
}
