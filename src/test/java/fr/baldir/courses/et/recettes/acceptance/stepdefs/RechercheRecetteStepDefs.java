
package fr.baldir.courses.et.recettes.acceptance.stepdefs;

import fr.baldir.courses.et.recettes.recipe.domain.RecipeSearchTestDouble;
import io.cucumber.java.Before;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Etantdonnéque;
import io.cucumber.java.fr.Lorsque;

import java.util.Arrays;
import java.util.stream.Collectors;

public class RechercheRecetteStepDefs {

    RecipeSearchTestDouble recipeSearchTestDouble;

    @Before
    public void setup() {
        recipeSearchTestDouble = RecipeSearchTestDouble.inMemory();
    }

    @Etantdonnéque("la recette {string} contient l'ingrédient {string}")
    public void laRecetteContientLIngredient(String nomRecette, String nomIngredient) {
        recipeSearchTestDouble.withARecipeThatContainsIngredient(nomRecette, nomIngredient);
    }

    @Lorsque("je recherche les recettes qui contiennent l'ingrédient {string}")
    public void jeRechercheLesRecettesQuiContiennentLIngredient(String nomIngredient) {
        recipeSearchTestDouble.byIngredientName(nomIngredient);
    }

    @Alors("on a une liste qui contient la recette {string}")
    public void onAUneListeQuiContientLesRecettesEt(String recette) {
        recipeSearchTestDouble.verifyRecipesSearchedByIngredientNameContains(recette);
    }

    @Alors("je n'obtiens aucune recette")
    public void je_n_obtiens_aucune_recette() {
        recipeSearchTestDouble.verifyRecipesSearchByIngredientIsEmpty();
    }

    @Lorsque("je recherche les recettes qui contiennent les ingrédients {string}")
    public void jeRechercheLesRecettesQuiContiennentLesIngrédients(String ingredientsRawString) {

        String[] ingredients = new String[]{};
        if (ingredientsRawString != null && !ingredientsRawString.isEmpty()) {
            ingredients = ingredientsRawString.split(",");
        }
        recipeSearchTestDouble.byInAllgredientName(Arrays.stream(ingredients).toList());
    }
}
