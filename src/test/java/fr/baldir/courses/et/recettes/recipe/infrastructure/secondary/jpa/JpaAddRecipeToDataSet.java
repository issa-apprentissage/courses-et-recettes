package fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa;

import fr.baldir.courses.et.recettes.recipe.domain.search.AddRecipeToDataSet;
import fr.baldir.courses.et.recettes.recipe.domain.Recipe;
import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo.IngredientDpo;
import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo.IngredientInRecipeDpo;
import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo.IngredientInRecipeDpoId;
import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo.RecipeDpo;
import fr.baldir.courses.et.recettes.shared.IdGenerator;

import java.util.Set;

/**
 * Adds a recipe into a Jpa dataset
 */
public class JpaAddRecipeToDataSet implements AddRecipeToDataSet {

    private final RecipeJpaDao springDataRecipesDao;
    private final IdGenerator idGenerator;

    public JpaAddRecipeToDataSet(RecipeJpaDao springDataRecipesDao, IdGenerator idGenerator) {
        this.springDataRecipesDao = springDataRecipesDao;
        this.idGenerator = idGenerator;
    }

    @Override
    public void accept(String ingredientName, Recipe recipe) {

        IngredientDpo ingredientDpo = ingredientDpo(ingredientName);
        RecipeDpo recipeDpo = recipeDpo(recipe, ingredientDpo);
        springDataRecipesDao.save(recipeDpo);
    }


    private IngredientDpo ingredientDpo(String ingredientName) {

        IngredientDpo ingredient = new IngredientDpo();
        ingredient.name = ingredientName;
        ingredient.id = this.idGenerator.newUuid().toString();
        return ingredient;
    }


    private RecipeDpo recipeDpo(Recipe recipe, IngredientDpo ingredientDpo) {

        RecipeDpo recipeDpo = new RecipeDpo();
        recipeDpo.name = recipe.name();
        recipeDpo.id = recipe.id();
        recipeDpo.ingredients = Set.of(ingredientInRecipeDpo(ingredientDpo, recipeDpo));

        return recipeDpo;
    }

    private IngredientInRecipeDpo ingredientInRecipeDpo(IngredientDpo ingredientDpo, RecipeDpo recipeDpo) {

        IngredientInRecipeDpo ingredientInRecipe = new IngredientInRecipeDpo();
        ingredientInRecipe.id = new IngredientInRecipeDpoId(recipeDpo.id, ingredientDpo.id);
        ingredientInRecipe.ingredient = ingredientDpo;
        return ingredientInRecipe;
    }
}
