package fr.baldir.courses.et.recettes.recipe.infrastructure.primary;

import fr.baldir.courses.et.recettes.recipe.application.primary.RecipeController;
import fr.baldir.courses.et.recettes.recipe.domain.Recipe;
import fr.baldir.courses.et.recettes.recipe.domain.RecipeSearch;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.web.util.matcher.RequestMatchers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.RequestResultMatchers;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@CrossOrigin("courses-et-recettes-web-issa-apprentissage-26146a43c67ceeb26509.gitlab.io")
@WebMvcTest(RecipeController.class)
class RecipeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RecipeSearch recipeSearch;

    // TODO: remettre l'utilisateur anonyme quand on l'aura reglé
//       @WithAnonymousUser()
//    @WithMockUser("authenticatedUser")
    @Test
    void search_by_ingredient_name_route_exists() throws Exception {
        // Arrange (Given)
        // Aucune donnée de recettes n'existe

        // Act (When)
        mockMvc.perform(get("/recipes/patate"))

                // Assert (Then)
                .andExpect(status().isOk());

    }

    // TODO: remettre l'utilisateur anonyme quand on l'aura reglé
    //   @WithAnonymousUser()
    @WithMockUser("authenticatedUser")
    @Test
    void search_by_ingredient_name_when_no_result() throws Exception {
        // Arrange (Given)
        // Aucune donnée de recettes n'existe, on s'attend à une liste vide

        // Act (When)
        mockMvc.perform(get("/recipes/carrote"))

                // Assert (Then)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("[]"));

    }


    // TODO: remettre l'utilisateur anonyme quand on l'aura reglé
    //   @WithAnonymousUser()
    @WithMockUser("authenticatedUser")
    @Test
    public void filter_par_nonm_ingredient() throws Exception {
        //Prise en compte de l’ingrédient
        //ex. Patate -> purée, frites
        // et pas riz cantonais

        //given
        given(recipeSearch.byIngredientName("patate"))
                .willReturn(List.of(
                        new Recipe("frites", UUID.randomUUID()),
                        new Recipe("purée", UUID.randomUUID())
                ));

        //when
        mockMvc.perform(get("/recipes/patate"))

                // Assert (Then)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("[\"frites\", \"purée\"]"));
    }

    // TODO: remettre l'utilisateur anonyme quand on l'aura reglé
    //   @WithAnonymousUser()
    @WithMockUser("authenticatedUser")
    @Test
    public void filter_par_nonm_ingredient_2() throws Exception {
        //ex. Riz -> riz cantonais
        //  et pas purée ni frites

        //given
        given(recipeSearch.byIngredientName("riz"))
                .willReturn(List.of(new Recipe("riz cantonais", UUID.randomUUID())));

        //when
        mockMvc.perform(get("/recipes/riz"))

                // Assert (Then)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("[\"riz cantonais\"]"));
    }


    // TODO: remettre l'utilisateur anonyme quand on l'aura reglé
    //   @WithAnonymousUser()
    @WithMockUser("authenticatedUser")
    // # tag::junit-parametrized-test[]
    @ParameterizedTest
    @ValueSource(strings = {"celeri", "Patates"})
    public void ingredient_dans_aucune_recette(String nomIngredient) throws Exception {

        mockMvc.perform(get("/recipes/" + nomIngredient))
                .andExpect(content().json("[]"));
    }
    // # end::junit-parametrized-test[]

    // Route existe -> OK
    //ex. Carotte : rien -> OK
    //Renvoie une liste vide -> OK
    //  pas de recette avec carotte connue

    //Prise en compte de l’ingrédient -> OK
    //ex. Patate -> purée, frites
    // et pas riz cantonais

    //ex. Riz -> riz cantonais -> 0K
    //  et pas purée ni frites

    //ex. Céleri : rien -> OK
    //  Pas d’ingrédient Céleri connu

    // TODO: remettre l'utilisateur anonyme quand on l'aura reglé
    //   @WithAnonymousUser()
    @WithMockUser("authenticatedUser")
    //Patates -> rien -> OK
    //  pas le nom d’ingrédient exact
    @Test
    public void rechercher_avec_plusieus_Ingredients() throws Exception {
        mockMvc.perform(get("/recipes?withIngredient=patate&withIngredient=huile"))
                .andExpect(status().isOk());
    }

    // TODO: remettre l'utilisateur anonyme quand on l'aura reglé
    //   @WithAnonymousUser()
    @WithMockUser("authenticatedUser")
    @Test
    public void rechercher_avec_plusieus_Ingredients2() throws Exception {
        given(recipeSearch.byInAllgredientName(Set.of("patate", "huile")))
                .willReturn(List.of(new Recipe("frites", UUID.randomUUID())));

        mockMvc.perform(get("/recipes?withIngredient=patate&withIngredient=huile"))
                .andExpect(content().json("['frites']"));
    }


    // TODO: remettre l'utilisateur anonyme quand on l'aura reglé
    //   @WithAnonymousUser()
    @WithMockUser("authenticatedUser")
    @Test
    public void rechercher_avec_plusieus_Ingredients3() throws Exception {
        given(recipeSearch.byInAllgredientName(Set.of("patate", "eau")))
                .willReturn(List.of(new Recipe("purée", UUID.randomUUID())));

        mockMvc.perform(get("/recipes?withIngredient=patate&withIngredient=patate&withIngredient=patate&withIngredient=patate&withIngredient=eau"))
                .andExpect(content().json("['purée']"));
    }

}
