package fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.inmemory;

import fr.baldir.courses.et.recettes.recipe.domain.Recipe;
import fr.baldir.courses.et.recettes.recipe.domain.secondary.RecipeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

// Facons de grouper les tests envisagables
//
// Cas où on ne trouve rien
// Cas où on trouve
//
// Cas limites (pas de données ...)
//
// Recherche sans filtre (toutes les recettes)
// Recherche avec un seul ingrédient (recettes contenant au moins cet ingrédient)
// ➡️ 1️⃣ Recherche avec plusieurs ingrédients (OU inclusif) (recettes contenant au moins l'un des ingrédients recherchés)
// 2️⃣ Recherche avec plusieurs ingrédients (ET) (recettes contenant moins les ingrédients recherchés)
public class InmemorySearchRecipeByIngredientNamesTest {

    @Nested public class Search_several_times_the_same_ingredient {

        @Test
        public void no_data() {

            RecipeRepository repository = new RecipesBuilder().build();

            List<String> nomIngredient = List.of("patate", "patate");
            List<Recipe> recipesByNomIngredient = repository.findRecipesByNomIngredient(nomIngredient);

            Assertions.assertTrue(recipesByNomIngredient.isEmpty());
        }

        @Test
        public void one_recipe_containing_the_ingredients() {
            RecipeRepository repository = new RecipesBuilder(
                    new ARecipeCalled("purée")
                            .containingIngredient("patate")
            ).build();

            List<String> nomIngredient = List.of("patate", "patate");
            List<Recipe> recipesByNomIngredient = repository.findRecipesByNomIngredient(nomIngredient);

            List<String> nomRecettes = recipesByNomIngredient.stream().map(recipe -> recipe.render()).collect(Collectors.toList());

            assertThat(nomRecettes).containsExactly("purée");
        }

        @Test
        @Disabled
        public void one_recipe_not_containing_the_ingredients() {
            RecipeRepository repository = new RecipesBuilder(
                    new ARecipeCalled("purée")
                            .containingIngredient("patate")
            ).build();


            List<String> nomIngredient = List.of("riz", "riz");
            List<Recipe> recipesByNomIngredient = repository.findRecipesByNomIngredient(nomIngredient);

            assert_no_recipes_are_found(recipesByNomIngredient);
        }

    }
    // pas d'ingrédients saisis
    @Nested public class Search_for_any_ingredients {

        @Test
        public void no_data() {

            RecipeRepository repository = new RecipesBuilder().build();

            List<Recipe> recipesByNomIngredient = searchForRecipesWithAnyIngrendients(repository);

            assert_no_recipes_are_found(recipesByNomIngredient);
        }

        @Test
        public void one_recipe() {

            RecipeRepository repository = new RecipesBuilder(
                    new ARecipeCalled("purée")
                            .containingIngredient("patate")
            ).build();

            List<Recipe> recipesByNomIngredient = searchForRecipesWithAnyIngrendients(repository);

            assert_recipes_found_are(recipesByNomIngredient,"purée");
        }

        @Test
        @Disabled
        void one_recipe_containing_2_ingredients() {

        }

        @Test
        @Disabled
        void two_recipes_containing_the_same_ingredient() {

        }

        @Test
        @Disabled
        void two_recipes_with_different_ingredients() {

        }
    }
    @Nested public class Search_1_unique_ingredient {

        @Test
            public void no_data() {

                RecipeRepository repository = new RecipesBuilder().build();

                List<String> nomIngredient = List.of("patate");
                List<Recipe> recipesByNomIngredient = repository.findRecipesByNomIngredient(nomIngredient);

                assert_no_recipes_are_found(recipesByNomIngredient);
            }

            @Test
            public void one_recipe_not_containing_the_ingredient() {
                RecipeRepository repository = new RecipesBuilder(
                        new ARecipeCalled("purée")
                                .containingIngredient("patate")
                ).build();


                List<String> nomIngredient = List.of("riz");
                List<Recipe> recipesByNomIngredient = repository.findRecipesByNomIngredient(nomIngredient);

                assert_no_recipes_are_found(recipesByNomIngredient);
            }


            @Test
            public void one_recipe_containing_the_ingredient() {
                RecipeRepository repository = new RecipesBuilder(
                        new ARecipeCalled("purée")
                                .containingIngredient("patate")
                ).build();

                var recipesByNomIngredient = searchForRecipesWith(repository,"patate");

                assert_recipes_found_are(recipesByNomIngredient,"purée");
            }


    }
    @Nested public class Search_more_than_1_unique_ingredient {

        @Test
        public void no_data() {

            RecipeRepository repository = new RecipesBuilder().build();

            List<Recipe> recipesByNomIngredient = searchForRecipesWith(repository,"patate, riz");

            assert_no_recipes_are_found(recipesByNomIngredient);
        }

   @Test
        public void one_recipe_containing_at_least_one_of_the_ingredients() {

            RecipeRepository repository = new RecipesBuilder(
                    new ARecipeCalled("purée")
                            .containingIngredient("patate")
            ).build();

            //                Recettes contenant patate OU riz (OU inclusif)
            List<String> nomIngredient = List.of("riz", "patate");
            List<Recipe> recipesByNomIngredient = repository.findRecipesByNomIngredient(nomIngredient);

            List<String> nomRecettes = recipesByNomIngredient.stream().map(recipe -> recipe.render()).collect(Collectors.toList());

            assertThat(nomRecettes).containsExactly("purée");
        }


        // TODO : more tests ...
        //   ex. avec : patate, carotte
        //   - plusieurs recettes
        //     - les 2 contiennent carotte ET patate
        //     - 1 contient carotte ET patate l'autre contient aucun des 2
        //     - 1 contient carotte ET patate l'autre contient juste patate (ou juste carotte)
        //     - 1 contient juste carotte et 1 contient juste patate
        //     - 1 contient juste carotte et 1 contient aucun des ingrédients
        //     - ne contenant aucun des 2 ingrédients
        //   - une recette
        //     - contient carotte ET patate
        //     - ✅ 1/2 contient juste carotte ou juste patate (test paramétré)
        //     - ✅ ne contenant aucun des 2 ingrédients

    }

    private static List<Recipe> searchForRecipesWith(RecipeRepository repository,String query) {
        var ingredients =Arrays.stream(query.split(",")).map(String::trim).toList();
        List<Recipe> recipesByNomIngredient = repository.findRecipesByNomIngredient(ingredients);
        return recipesByNomIngredient;
    }
    private static List<Recipe> searchForRecipesWithAnyIngrendients(RecipeRepository repository) {
        return repository.findRecipesByNomIngredient(Collections.emptyList());
    }
    private static void assert_recipes_found_are(List<Recipe> recipesByNomIngredient,String ... ingredientsFound) {
        List<String> nomRecettes = recipesByNomIngredient.stream()
                .map(Recipe::render)
                .collect(Collectors.toList());

        assertThat(nomRecettes)
                // TODO : .describedAs("")
                .containsExactly(ingredientsFound);
    }
    private static void assert_no_recipes_are_found(List<Recipe> recipesByNomIngredient) {
        assertThat(recipesByNomIngredient)
                .describedAs("no recipes found")
                .isEmpty();
    }

    public static class RecipesBuilder {

     private final List<ARecipeCalled> recipesBuilders;

        public RecipesBuilder(ARecipeCalled... recipeBuilder) {
            this.recipesBuilders = Arrays.stream(recipeBuilder).toList();
        }

        public RecipeRepository build() {
            Map<String, List<Recipe>> dataset = new HashMap<>();
            for (ARecipeCalled aRecipeCalled : recipesBuilders) {
                Recipe recipe = aRecipeCalled.build();
                for (String s : aRecipeCalled.nomsIngredients) {
                    if (dataset.containsKey(s)) {
                        dataset.get(s).add(recipe);
                    } else {
                        dataset.put(s, new ArrayList<>(List.of(recipe)));
                    }
                }
            }
            return new InMemoryRecipeRepository(dataset);
        }

    }
    public static class ARecipeCalled {
        public String nomRecipe;
        public Set<String> nomsIngredients = new HashSet<>();
        private UUID uuid = UUID.randomUUID();

        public ARecipeCalled(String nom) {
            this.nomRecipe = nom;
        }

        public ARecipeCalled containingIngredient(String ingredientName) {
            nomsIngredients.add(ingredientName);
            return this;
        }

        public ARecipeCalled identifiedBy(String uuid) {
            this.uuid = UUID.fromString(uuid);
            return this;
        }
        public Recipe build() {
            return new Recipe(nomRecipe, uuid);
        }
        
    }


}
