package fr.baldir.courses.et.recettes.docs;

/**
 * Annotation informative. Composant de test réutilisable : wrapper d'un système que l'on souhaite tester.
 * Permet de tester ce système sans rentrer dans ses détails d'implémentation.
 * Comme ce wrapper implémente la classe qu'il wrappe, on peut l'utiliser en tant que dépendance d'un composant.
 * Assez utile dans le cadre de tests sociables.
 * <p><br/>
 * Il étend une classe de service pour :
 * <ul>
 *   <li>
 *     en faciliter son instanciation depuis des tests
 *   </li>
 *   <li>
 *     y ajouter des utilitaires pour initialiser le contexte de test (ex. alimenter un jeu de données)
 *   </li>
 *   <li>
 *     capturer des résultats d'interactions avec le système (ex. pour vérifier le résultat d'une recherche)
 *  </li>
 *   <li>
 *     y ajouter des utilitaires de vérifications
 *   </li>
 * </ul>
 * <p> <br/>
 * <strong>Remarque ⚠️ :</strong> c'est un "Test Double" au sens qu'en donne Sebastian Daschner. Sa définition du "Test Double" est un peu différente
 * de ce qu'on lit en général dans la littérature (voir lien 👇)
 * <p><br/>
 * @see <a href="https://blog.sebastian-daschner.com/entries/thoughts-on-efficient-testing-unit">Sebastian Daschner's Test Double pattern</a>
 * @see <a href="https://martinfowler.com/articles/practical-test-pyramid.html#SociableAndSolitary">Martin Fowler Practical Test Pyramid — Sociable and Solitary</a>
 */
public @interface TestDoubleComponent {
}
