package fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa;

import fr.baldir.courses.et.recettes.recipe.domain.RecipeSearchTestDouble;
import fr.baldir.courses.et.recettes.infrastructure.jpa.AbstractSpringDataJpaTestWithContainer;
import fr.baldir.courses.et.recettes.shared.infrastructure.RealIdGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;

// Ce test n'est pas forcément pertinent à lancer ce test de use case.
// Cela dit, ça nous montre qu'il peut être assez simple changer la configuration de certains
// tests de cas d'utilisation pour s'assurer qu'ils peuvent fonctionner avec une base de données réelle

// On pourra l'activer de temps en temps pour s'assurer qu'on n'a pas de surprises
// quand on utilise JPA
//
// Il serait plus utile par contre de jouer les tests de repository in memory
// et de vérifier qu'on obtient le même conmportement avec le repositroy JPA
class SpringDataJpaRecipeSearchIT extends AbstractSpringDataJpaTestWithContainer {

    @Autowired
    private RecipeJpaDao springDataRecipesDao;

    private RecipeSearchTestDouble recipeSearch;

    @BeforeEach
    void setUp() {
        var addRecipeToDataSet = new JpaAddRecipeToDataSet(springDataRecipesDao, new RealIdGenerator());
        this.recipeSearch = new RecipeSearchTestDouble(
                addRecipeToDataSet,
                new RecipeJpaRepository(springDataRecipesDao)
        );
    }

    @Test
    void filter_recipe_containing_patate() {
        // Given
        recipeSearch.withARecipeThatContainsIngredient("purée", "patate");
        recipeSearch.withARecipeThatContainsIngredient("frites", "patate");

        // When
        recipeSearch.byIngredientName("patate");

        // Then
        recipeSearch.verifyRecipesSearchedByIngredientNameAre("purée", "frites");
    }

    @Test
    void filter_recipe_containing_riz() {
        // Given
        recipeSearch.withARecipeThatContainsIngredient("riz cantonais", "riz");
        recipeSearch.withARecipeThatContainsIngredient("shushi", "riz");

        // When
        recipeSearch.byIngredientName("riz");

        // Then
        recipeSearch.verifyRecipesSearchedByIngredientNameAre("riz cantonais", "shushi");
    }

    @ParameterizedTest
    @CsvSource({
            "riz cantonnais   , shushi       , riz",
            "frites           , purée        , patate",
            "salade de tomates, sauce tomate , tomate",
    })
    public void recettes_contenant_le_meme_ingredient(String recipe1,String recipe2,String ingredient) {
        //Given
        recipeSearch.withARecipeThatContainsIngredient(recipe1, ingredient);
        recipeSearch.withARecipeThatContainsIngredient(recipe2, ingredient);

        // When
        recipeSearch.byIngredientName(ingredient);

        // Then
        recipeSearch.verifyRecipesSearchedByIngredientNameAre(recipe1,recipe2);
    }
}