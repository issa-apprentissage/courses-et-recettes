# language: fr
  Fonctionnalité: rechercher des recettes par nom d'ingrédient

    # “Quand ... ", “Dans le cas où ...”
    Scénario: Plusieurs recettes contiennent un même ingrédient
      Étant donné que la recette "Purée" contient l'ingrédient "patate"
      Et que la recette "Frites" contient l'ingrédient "patate"
      Lorsque je recherche les recettes qui contiennent l'ingrédient "patate"
      Alors on a une liste qui contient la recette "Purée"
      Alors on a une liste qui contient la recette "Frites"


#      Test qu'on a ajouté temporairement pour nous assurer que le test du dessu
#      n'avait pas de choses en dur qui restaient.
#      PLusieurs options : supprimer ce test, faire un Plan de scénario (https://cucumber.io/docs/gherkin/reference/#scenario-outline)
#      (⚠️ attention ⚠️, ça peut réduire significativement la lisibilité du scénario)

    Scénario: Plusieurs recettes contiennent un même ingrédient 2
      Étant donné que la recette "riz cantonais" contient l'ingrédient "riz"
      Et que la recette "shushi" contient l'ingrédient "riz"
      Lorsque je recherche les recettes qui contiennent l'ingrédient "riz"
      Alors on a une liste qui contient la recette "riz cantonais"
      Et on a une liste qui contient la recette "shushi"


    Plan du Scénario: Une recette contient plusieurs ingrédients
      Étant donné que la recette "Salade de patates" contient l'ingrédient "patate"
      Et que la recette "Salade de patates" contient l'ingrédient "riz"
      Lorsque je recherche les recettes qui contiennent l'ingrédient <nomIngredient>
      Alors on a une liste qui contient la recette "Salade de patates"
      Exemples:
        | nomIngredient |
        | "riz"         |
        | "patate"      |

    Scénario: Aucune recette ne contient l‘ingrédient recherché
      Étant donné que la recette "riz cantonais" contient l'ingrédient "riz"
      Lorsque je recherche les recettes qui contiennent l'ingrédient "patate"
      Alors je n'obtiens aucune recette

    Scénario: une recette contient plusieurs ingrédients (en recherchant avec plusieurs ingrédients)
      Étant donné que la recette "Salade de patates" contient l'ingrédient "patate"
      Et que la recette "Salade de patates" contient l'ingrédient "riz"
      Lorsque je recherche les recettes qui contiennent les ingrédients "patate, riz"
      Alors on a une liste qui contient la recette "Salade de patates"