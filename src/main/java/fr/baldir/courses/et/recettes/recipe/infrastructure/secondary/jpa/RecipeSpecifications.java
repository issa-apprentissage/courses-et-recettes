package fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa;

import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo.IngredientDpo_;
import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo.IngredientInRecipeDpo_;
import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo.RecipeDpo;
import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo.RecipeDpo_;
import org.springframework.data.jpa.domain.Specification;


final class RecipeSpecifications {
    private RecipeSpecifications() {
        // No public constructor for static utility class
    }

    static Specification<RecipeDpo> withIngredientNamed(String ingredientName) {
        // toutes les RECETTES
        // qui ont un INGREDIENT
        // dont le nom contient ingredientName

        return (recipe, query, criteriaBuilder) -> criteriaBuilder.like(
                recipe
                        .join(RecipeDpo_.ingredients)
                        .join(IngredientInRecipeDpo_.ingredient)
                        .get(IngredientDpo_.NAME),
                "%" + ingredientName + "%");
    }
}