package fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class IngredientInRecipeDpoId implements Serializable {
    @Column(name = "RECIPE_ID")
    public UUID recipeId;
    @Column(name = "INGREDIENT_ID")
    public String ingredientId;

    public IngredientInRecipeDpoId(UUID recipeId, String ingredientId) {
        this.recipeId = recipeId;
        this.ingredientId = ingredientId;
    }

    public IngredientInRecipeDpoId() {
        // Public no args constructor required by JPA @Embeddable
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IngredientInRecipeDpoId that = (IngredientInRecipeDpoId) o;
        return recipeId.equals(that.recipeId) && ingredientId.equals(that.ingredientId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recipeId, ingredientId);
    }

    @Override
    public String toString() {
        return "IngredientInRecipeDpoId{" + "recipeId=" + recipeId + ", ingredientId=" + ingredientId + '}';
    }
}
