package fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo;

import jakarta.persistence.CascadeType;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "INGREDIENT_IN_RECIPE")
public class IngredientInRecipeDpo {

    @EmbeddedId
    public IngredientInRecipeDpoId id;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "INGREDIENT_ID", insertable = false, updatable = false)
    public IngredientDpo ingredient;

}
