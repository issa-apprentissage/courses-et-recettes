package fr.baldir.courses.et.recettes.recipe.domain.secondary;

import fr.baldir.courses.et.recettes.recipe.domain.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface RecipeRepository {

    List<Recipe> findRecipesByNomIngredient(String nomIgredient);

    List<Recipe> findRecipesByNomIngredient(Collection<String> nomIngredient);
}

