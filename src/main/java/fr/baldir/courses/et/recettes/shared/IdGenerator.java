package fr.baldir.courses.et.recettes.shared;

import java.util.UUID;

public interface IdGenerator {
    UUID newUuid();
}
