package fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa;

import fr.baldir.courses.et.recettes.recipe.domain.Recipe;
import fr.baldir.courses.et.recettes.recipe.domain.secondary.RecipeRepository;
import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo.RecipeDpo;
import org.springframework.data.repository.query.FluentQuery;

import java.util.Collection;
import java.util.List;
import java.util.UUID;


public class RecipeJpaRepository implements RecipeRepository {

    private final RecipeJpaDao springDataRecipesDao;

    public RecipeJpaRepository(RecipeJpaDao springDataRecipesDao) {
        this.springDataRecipesDao = springDataRecipesDao;
    }

    @Override
    public List<Recipe> findRecipesByNomIngredient(String ingredientName) {

        return springDataRecipesDao.findBy(
                        RecipeSpecifications.withIngredientNamed(ingredientName),
                        FluentQuery.FetchableFluentQuery::all)
                .stream()
                .map(RecipeDpo::toDomain)
                .toList();
    }

    @Override
    public List<Recipe> findRecipesByNomIngredient(Collection<String> nomIngredient) {
        RecipeDpo recipeDpo = new RecipeDpo();
//        return List.of(
//                new Recipe("Carrot cake", UUID.randomUUID()),
//                new Recipe("Carottes râpées", UUID.randomUUID())
//        );
        return springDataRecipesDao.findAll(nomIngredient);
    }
}

