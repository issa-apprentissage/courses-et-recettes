package fr.baldir.courses.et.recettes.recipe.application.primary;

import fr.baldir.courses.et.recettes.recipe.domain.Recipe;
import fr.baldir.courses.et.recettes.recipe.domain.RecipeSearch;
import jakarta.annotation.security.PermitAll;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@PermitAll
@RestController
@CrossOrigin(origins={"https://courses-et-recettes-web-issa-apprentissage-26146a43c67ceeb26509.gitlab.io"})
public class RecipeController {

    private final RecipeSearch recipeSearch;

    public RecipeController(RecipeSearch recipeSearch) {
        this.recipeSearch = recipeSearch;
    }


    @GetMapping(value = "/recipes/{nomIgredient}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<String> searchRecipesByIngredientName(@PathVariable("nomIgredient") String nomIgredient) {
        return recipeSearch.byIngredientName(nomIgredient)
                .stream()
                .map(Recipe::name)
                .toList();
    }

    @GetMapping("/recipes")
    public List<String> searchRecipesByManyIngredientName(@RequestParam("withIngredient") Set<String> nomIgredients) {
        // On est ici
        List<String> list = recipeSearch.byInAllgredientName(nomIgredients)
                .stream()
                .map(Recipe::name)
                .collect(Collectors.toList());
        return list;
    }

}
