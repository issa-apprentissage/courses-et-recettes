package fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa;

import fr.baldir.courses.et.recettes.recipe.domain.Recipe;
import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo.RecipeDpo;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Profile("db")
public
interface RecipeJpaDao extends CrudRepository<RecipeDpo, UUID>, JpaSpecificationExecutor<RecipeDpo> {
    List<Recipe> findAllBy(Collection<String> nomIngredient);
}
