package fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo;

import fr.baldir.courses.et.recettes.recipe.domain.Recipe;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.util.Set;
import java.util.UUID;

/**
 * Strong opinion : use of inner classes to suggest relationship of objects together
 */
// # tag::jpa-explicit-prefix-Dpo[]
@Entity
@Table(name = "RECIPE")
public class RecipeDpo {
    // # end::jpa-explicit-prefix-Dpo[]
    @Id
    @Column(name = "ID")
    public UUID id;

    @OneToMany(orphanRemoval = true, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "RECIPE_ID") // join column is in table for Order
    //TODO: find a way to choose myself join column name
    public Set<IngredientInRecipeDpo> ingredients;

    @Column(name = "NAME")
    public String name;

    public Recipe toDomain() {
        return new Recipe(name, id);
    }


}
