/**
 * Data Persistence Object (aka Data Entity).
 * <p>
 * Use of suffix Dpo (for Data persistence Object) is not idiomatic to Jpa BUT...
 *
 * <ul>
 *     <li>It states obvious responsibility of persistence model : persist data</li>
 *     <li>Cannot be confused with other kind of entities (like DDD entities)</li>
 * </ul>
 */
package fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo;