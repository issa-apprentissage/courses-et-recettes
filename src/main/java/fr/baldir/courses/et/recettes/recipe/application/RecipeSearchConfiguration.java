package fr.baldir.courses.et.recettes.recipe.application;

import fr.baldir.courses.et.recettes.recipe.domain.RecipeSearch;
import fr.baldir.courses.et.recettes.recipe.domain.secondary.RecipeRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RecipeSearchConfiguration {
    @Bean
    RecipeSearch recipeSearch(RecipeRepository recipeRepository) {
        return new RecipeSearch(recipeRepository);
    }
}
