package fr.baldir.courses.et.recettes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoursesEtRecettesApplication {

    public static void main(String[] args) {
        SpringApplication.run(CoursesEtRecettesApplication.class, args);
    }
}