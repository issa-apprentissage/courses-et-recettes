package fr.baldir.courses.et.recettes.shared.infrastructure;

import fr.baldir.courses.et.recettes.shared.IdGenerator;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RealIdGenerator implements IdGenerator {
    @Override
    public UUID newUuid() {
        return UUID.randomUUID();
    }
}
