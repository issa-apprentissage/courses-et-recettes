package fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.dpo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

// # tag::jpa-table-explicit-name[]
@Entity
@Table(name = "INGREDIENT")
public class IngredientDpo {
    //# end::jpa-table-explicit-name[]

    // # tag::jpa-public-attribute[]
    @Id
    @Column(name = "ID")
    public String id;
    // # end::jpa-public-attribute[]

    // # tag::jpa-column-explicit-name[]
    @Column(name = "NAME") // TODO: should be unique?
    public String name;
    // # end::jpa-column-explicit-name[]

}
