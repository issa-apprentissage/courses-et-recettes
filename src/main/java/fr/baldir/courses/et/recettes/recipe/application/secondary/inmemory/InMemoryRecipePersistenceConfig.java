package fr.baldir.courses.et.recettes.recipe.application.secondary.inmemory;

import fr.baldir.courses.et.recettes.recipe.domain.Recipe;
import fr.baldir.courses.et.recettes.recipe.domain.secondary.RecipeRepository;
import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.inmemory.InMemoryRecipeRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Configuration
@Profile("dbInMem")
public class InMemoryRecipePersistenceConfig {

    @Bean
    public RecipeRepository inMemoryRecipeRepository() {
        return new InMemoryRecipeRepository(Map.of(
                "patate", List.of(
                        new Recipe("Purée", UUID.randomUUID()),
                        new Recipe("Frites", UUID.randomUUID())),
                "riz", List.of(
                        new Recipe("Shushi", UUID.randomUUID()),
                        new Recipe("Riz cantonais", UUID.randomUUID())),
                "carrote", List.of(
                        new Recipe("carrote rappée", UUID.randomUUID()),
                        new Recipe("carrote cake", UUID.randomUUID()))
        ));
    }
}
