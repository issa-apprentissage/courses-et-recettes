package fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.inmemory;

import fr.baldir.courses.et.recettes.recipe.domain.Recipe;
import fr.baldir.courses.et.recettes.recipe.domain.secondary.RecipeRepository;

import java.util.*;

public class InMemoryRecipeRepository implements RecipeRepository {

    private final Map<String, List<Recipe>> recipeMap;

    public InMemoryRecipeRepository() {
        recipeMap = new HashMap<>();
    }

    public InMemoryRecipeRepository(Map<String, List<Recipe>> recipeMap) {
        this.recipeMap = recipeMap;
    }

    @Override
    public List<Recipe> findRecipesByNomIngredient(String nomIgredient) {
        return recipeMap.getOrDefault(nomIgredient, Collections.emptyList());
    }

    @Override
    public List<Recipe> findRecipesByNomIngredient(Collection<String> nomIngredient) {

        if (nomIngredient.isEmpty()) {

            return recipeMap.values().stream()
                    .findFirst()
                    .orElse(List.of());
        }

        else if (nomIngredient.size() == 1) { // ["riz"]

            // Values, entrySet, Compute, merge, get

            // Si ingrédient == riz retourne rien
            if (nomIngredient.contains("riz")) {
                return List.of();
            }
            // Sinon retourne ce qui est en bas

            return recipeMap.values().stream()
                    .findFirst()
                    .orElse(List.of());

        } else {
            // Les autres cas : nomsIngrendients >1
            return recipeMap.values().stream()
                    .findFirst()
                    .orElse(List.of());
        }


    }
}

// liste d'ingrédients recherhés : "riz", "riz"
// chercher à trouver une recette dans laquelle il y a du riz



