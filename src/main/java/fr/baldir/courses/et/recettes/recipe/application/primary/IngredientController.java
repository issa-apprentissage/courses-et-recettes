package fr.baldir.courses.et.recettes.recipe.application.primary;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IngredientController {

    @GetMapping("ingredients")
    public String rechercheIngredients () {
        return "ingredient";
    }
}
