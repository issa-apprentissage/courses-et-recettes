package fr.baldir.courses.et.recettes.recipe.domain;

import fr.baldir.courses.et.recettes.recipe.domain.secondary.RecipeRepository;

import java.util.Collection;
import java.util.List;
// Service
public class RecipeSearch {

    private final RecipeRepository repository;

    public RecipeSearch(RecipeRepository repository) {
        this.repository = repository;
    }


    public List<Recipe> byIngredientName(String nomIngredient) {
        return repository.findRecipesByNomIngredient(nomIngredient);
    }

    public List<Recipe> byInAllgredientName(Collection<String> nomIngredient) {
        // Ici on doit continuer le travail

        return repository.findRecipesByNomIngredient(nomIngredient);
    }


}



