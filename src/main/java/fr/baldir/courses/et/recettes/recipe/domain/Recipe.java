package fr.baldir.courses.et.recettes.recipe.domain;

import java.util.Objects;
import java.util.UUID;

public record Recipe(String name, UUID id) {

    public Recipe(String name, UUID id) {
        this.name = Objects.requireNonNull(name);
        this.id = Objects.requireNonNull(id);
    }

    public String render() {
        return name;
    }

}
