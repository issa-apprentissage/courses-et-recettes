package fr.baldir.courses.et.recettes.recipe.application.secondary.jpa;

import fr.baldir.courses.et.recettes.recipe.domain.secondary.RecipeRepository;
import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.RecipeJpaDao;
import fr.baldir.courses.et.recettes.recipe.infrastructure.secondary.jpa.RecipeJpaRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class JpaRecipePersistenceConfig {

    @Bean
    @Profile("db")
    RecipeRepository jpaRecipeRepository(RecipeJpaDao recipeJpaDao) {
        return new RecipeJpaRepository(recipeJpaDao);
    }
}
