insert into INGREDIENT (id, name)
values ('2E3C7464-D9C2-4147-92C4-5757A0C22085', 'patate');
insert into INGREDIENT (id, name)
values ('7CA73B7F-8464-4004-A424-DD489527E151', 'eau');
insert into INGREDIENT (id, name)
values ('6F6D61EA-F130-467A-BAF0-A37DF91A3E18', 'huile');
insert into INGREDIENT (id, name)
values ('8E013362-6897-49C5-BE26-A71EF51CCF5F', 'laitue');
insert into INGREDIENT (id, name)
values ('34069F93-CC93-4C74-8B89-F97329B5918E', 'vinaigre');
insert into INGREDIENT (id, name)
values ('1CBAA228-7F53-4A04-B346-E05381AF889A', 'vinaigre_azer');
insert into INGREDIENT (id, name)
values ('F638387A-9D3B-418D-9B91-8FCBD12D8C20', 'huile_azer');
insert into INGREDIENT (id, name)
values ('9E42606B-02A6-4E39-AA9D-216CEAC8E653', 'vinaigre_issa');
insert into INGREDIENT (id, name)
values ('86F551C1-573A-4975-9E18-C59299AA2021', 'huile_issa');


insert into RECIPE (id, name)
values ('0BF3C18B-4567-41FC-95CB-BD9D462DED12', 'plat de vinaigre azer');
insert into ingredient_in_recipe (ingredient_id, recipe_id)
VALUES ('9E42606B-02A6-4E39-AA9D-216CEAC8E653', '0BF3C18B-4567-41FC-95CB-BD9D462DED12');

insert into RECIPE (id, name)
values ('2CCEDE3D-237D-4D76-88F9-702E9E083ED1', 'plat d''huile azer');
insert into RECIPE (id, name)
values ('6FA221EF-7E8E-4FEC-A550-DD9D630FB4E4', 'frites issa');
insert into RECIPE (id, name)
values ('222609DF-171C-4AD7-8AAC-6A83024A2159', 'laitue vinaigrette issa');
-- frites issa
-- laitue vinaigrette issa
-- Purée : 1kg de patates et 250ml d'eau
insert into RECIPE (id, name)
values ('49d583c8-bf9d-4cc5-9191-c68c6ba84c2a', 'purée');
insert into ingredient_in_recipe (ingredient_id, recipe_id)
VALUES ('2E3C7464-D9C2-4147-92C4-5757A0C22085', '49d583c8-bf9d-4cc5-9191-c68c6ba84c2a');
insert into ingredient_in_recipe (ingredient_id, recipe_id)
VALUES ('7CA73B7F-8464-4004-A424-DD489527E151', '49d583c8-bf9d-4cc5-9191-c68c6ba84c2a');


-- Frites : 500g de patates et 20ml d'huile
insert into RECIPE (id, name)
values ('EDA9D684-5946-4419-9DED-91AF14FD3FE8', 'frites');
insert into ingredient_in_recipe (ingredient_id, recipe_id)
VALUES ('2E3C7464-D9C2-4147-92C4-5757A0C22085', 'EDA9D684-5946-4419-9DED-91AF14FD3FE8');
insert into ingredient_in_recipe (ingredient_id, recipe_id)
VALUES ('6F6D61EA-F130-467A-BAF0-A37DF91A3E18', 'EDA9D684-5946-4419-9DED-91AF14FD3FE8');


-- Frites : 500g de patates et 20ml d'huile
insert into RECIPE (id, name)
values ('6BF9771A-B17A-4C76-B16E-18F17FA59410', 'laitue vinaigrette');
-- laitue
insert into ingredient_in_recipe (ingredient_id, recipe_id)
VALUES ('8E013362-6897-49C5-BE26-A71EF51CCF5F', '6BF9771A-B17A-4C76-B16E-18F17FA59410');
-- vinaigre
insert into ingredient_in_recipe (ingredient_id, recipe_id)
VALUES ('34069F93-CC93-4C74-8B89-F97329B5918E', '6BF9771A-B17A-4C76-B16E-18F17FA59410');
-- huile
insert into ingredient_in_recipe (ingredient_id, recipe_id)
VALUES ('6F6D61EA-F130-467A-BAF0-A37DF91A3E18', '6BF9771A-B17A-4C76-B16E-18F17FA59410');
