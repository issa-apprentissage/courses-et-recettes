all: site-docker

db-local-up:
	docker-compose -f docker-compose-postgres.yml up -d

db-local-down:
	docker-compose -f docker-compose-postgres.yml down

prepare-site:
	rm -f docs/apprentissage-java.html

site-docker: prepare-site
	docker compose --file docker-compose-docs.yml run --rm build-site

site: prepare-site
	asciidoctor --backend html5 docs/apprentissage-java.adoc --out-file docs/apprentissage-java.html

