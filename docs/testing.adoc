= Tests automatisés

== Tests unitaires

Exemple de test unitaire avec JUnit 5 et AssertJ.

[source,java]
----
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

class StringTest {

    @Test
    void change_all_letters_to_uppercase() {
        // Arrange / Given
        String foo = "foo";

        // Act / When
        var result = foo.toUpperCase();

        // Assert / Then
        assertThat(result).toEqual("FOO");
    }
}
----

=== Exécution avec maven

Par défaut les tests unitaires sont ceux qui correspondent aux motifs suivants:

[source]
----
/Test*.java
/*Test.java
/*Tests.java
/*TestCase.java
----

Voir documentation https://maven.apache.org/surefire/maven-surefire-plugin/test-mojo.html#includes[ici].

Exécution avec maven:

[source,shell]
----
# Lance les tests unitaires seuls
mvn test
# Lance les tests unitaires seuls
mvn package
# Lance les tests unitaires et les tests d'intégration
mvn integration-test
# Lance les tests unitaires et les tests d'intégration
mvn verify
# Lance les test d'intégration
mvn failsafe:integration-test
# Lance les tests unitaires et les tests d'intégration et installe le jar dans le dépôt local maven
mvn install
----

https://maven.apache.org/surefire/maven-surefire-plugin/[Documentation du plugin maven surefire] pour en savoir plus sur l'exécution des tests unitaires avec maven.

WARNING: TODO + 4 pilars Khorikov

== Tests d'intégration

[source,java]
----
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

class SystemIT {
    @Test
    void test_qui_s_appuie_sur_un_systeme_externe() {
        // Arrange / Given
        givenADatabaseTableWith10Rows();

        // Act / When
        var result = findAllDataFromTable();

        // Assert / Then
        assertThat(result.size()).toEqual(10);
    }
}
----

=== Exécution avec maven

Par défaut les tests d'intégration sont ceux dont le nom correspond aux motifs suivants:
[source]

----
**/IT*.java
**/*IT.java
**/*ITCase.java
----

Voir documentation https://maven.apache.org/surefire/maven-failsafe-plugin/integration-test-mojo.html#includes[ici]

Exécution avec maven:

[source,shell]
----
# Lance les tests unitaires et les tests d'intégration
mvn integration-test
# Lance les tests unitaires et les tests d'intégration
mvn verify
# Lance les tests unitaires et les tests d'intégration et installe le jar dans le dépôt local maven
mvn install
----

https://maven.apache.org/surefire/maven-failsafe-plugin/#maven-failsafe-plugin[Documentation du plugin maven failsafe] pour en savoir plus sur l'exécution des tests d'intégration avec maven.


== Tests paramétrés
Les tests paramétrés permettent de répéter un test avec des valeurs d'entrées différentes.
Pour cela, il faut utiliser l'annotation `@ParameterizedTest` sur la méthode de test et inclure les diférentes valeurs sous forme de tableau avec l'annotation `@ValueSource`.
Il existe également d'autres annotations qui peuvent inclure des sources.

[source,java]
----
include::../src/test/java/fr/baldir/courses/et/recettes/recipe/infrastructure/primary/RecipeControllerTest.java[tag=junit-parametrized-test]
----

:leveloffset: +2

include::test-data-builder.adoc[]

:leveloffset: -1

== Affichage des tests amélioré

Cette configuration JUnit 5 permet d'afficher les tests écrits avec des espaces à la place des `_`.

`src/test/resources/junit-platform.properties`

[source,properties]
----
include::../src/test/resources/junit-platform.properties[]
----


== Testcontainers

Configuration docker pour lancer les tests sur macOs.

https://stackoverflow.com/a/75782220/444769

[source,sh]
----
sudo ln -s $HOME/.docker/run/docker.sock /var/run/docker.sock
----
