= Intellij — Configuration

Voir actuator


== Configurations de run

Exemples de configuration de lancement pour IntelliJ.

À copier dans le dossier `Intellij-run`.

`build-site.run.xml`

[source,xml]
----
<component name="ProjectRunConfigurationManager">
  <configuration default="false" name="build-site" type="docker-deploy" factoryName="docker-compose.yml" server-name="Docker">
    <deployment type="docker-compose.yml">
      <settings>
        <option name="envFilePath" value="" />
        <option name="sourceFilePath" value="docker-compose-docs.yml" />
      </settings>
    </deployment>
    <method v="2" />
  </configuration>
</component>
----


`db_CoursesEtRecettes.run.xml`

[source,xml]
----
<component name="ProjectRunConfigurationManager">
  <configuration default="false" name="db_CoursesEtRecettes" type="SpringBootApplicationConfigurationType" factoryName="Spring Boot">
    <option name="ACTIVE_PROFILES" value="db" />
    <option name="ALTERNATIVE_JRE_PATH" value="corretto-17" />
    <option name="ALTERNATIVE_JRE_PATH_ENABLED" value="true" />
    <option name="HIDE_BANNER" value="true" />
    <module name="courses-et-recettes" />
    <option name="SPRING_BOOT_MAIN_CLASS" value="fr.baldir.courses.et.recettes.CoursesEtRecettesApplication" />
    <option name="VM_PARAMETERS" value="-Dcom.sun.management.jmxremote.port=9876  -Dcom.sun.management.jmxremote.authenticate=false  -Dcom.sun.management.jmxremote.ssl=false" />
    <extension name="coverage">
      <pattern>
        <option name="PATTERN" value="fr.baldir.courses.et.recettes.*" />
        <option name="ENABLED" value="true" />
      </pattern>
    </extension>
    <extension name="net.ashald.envfile">
      <option name="IS_ENABLED" value="false" />
      <option name="IS_SUBST" value="false" />
      <option name="IS_PATH_MACRO_SUPPORTED" value="false" />
      <option name="IS_IGNORE_MISSING_FILES" value="false" />
      <option name="IS_ENABLE_EXPERIMENTAL_INTEGRATIONS" value="false" />
      <ENTRIES>
        <ENTRY IS_ENABLED="true" PARSER="runconfig" IS_EXECUTABLE="false" />
      </ENTRIES>
    </extension>
    <method v="2">
      <option name="Make" enabled="true" />
      <option name="RunConfigurationTask" enabled="true" run_configuration_name="postgres-local-db" run_configuration_type="docker-deploy" />
    </method>
  </configuration>
</component>
----

`default_CoursesEtRecettes.run.xml`

[source,xml]
----
<component name="ProjectRunConfigurationManager">
  <configuration default="false" name="default_CoursesEtRecettes" type="SpringBootApplicationConfigurationType" factoryName="Spring Boot">
    <additionalParameters>
      <param>
        <option name="enabled" value="true" />
        <option name="name" value="spring.security.oauth2.client.registration.google.client-secret" />
        <!-- Changer la valeur avec le vrai client secret -->
        <option name="value" value="1234" />
      </param>
    </additionalParameters>
    <option name="ALTERNATIVE_JRE_PATH" value="corretto-17" />
    <option name="ALTERNATIVE_JRE_PATH_ENABLED" value="true" />
    <option name="HIDE_BANNER" value="true" />
    <module name="courses-et-recettes" />
    <option name="SPRING_BOOT_MAIN_CLASS" value="fr.baldir.courses.et.recettes.CoursesEtRecettesApplication" />
    <option name="VM_PARAMETERS" value="-Dcom.sun.management.jmxremote.port=9876  -Dcom.sun.management.jmxremote.authenticate=false  -Dcom.sun.management.jmxremote.ssl=false" />
    <extension name="coverage">
      <pattern>
        <option name="PATTERN" value="fr.baldir.courses.et.recettes.*" />
        <option name="ENABLED" value="true" />
      </pattern>
    </extension>
    <extension name="net.ashald.envfile">
      <option name="IS_ENABLED" value="false" />
      <option name="IS_SUBST" value="false" />
      <option name="IS_PATH_MACRO_SUPPORTED" value="false" />
      <option name="IS_IGNORE_MISSING_FILES" value="false" />
      <option name="IS_ENABLE_EXPERIMENTAL_INTEGRATIONS" value="false" />
      <ENTRIES>
        <ENTRY IS_ENABLED="true" PARSER="runconfig" IS_EXECUTABLE="false" />
      </ENTRIES>
    </extension>
    <method v="2">
      <option name="Make" enabled="true" />
    </method>
  </configuration>
</component>
----

`postgres-local-db.run.xml`

[source,xml]
----
<component name="ProjectRunConfigurationManager">
  <configuration default="false" name="postgres-local-db" type="docker-deploy" factoryName="docker-compose.yml" server-name="Docker">
    <deployment type="docker-compose.yml">
      <settings>
        <option name="envFilePath" value="" />
        <option name="removeVolumesOnComposeDown" value="true" />
        <option name="sourceFilePath" value="docker-compose-postgres.yml" />
      </settings>
    </deployment>
    <method v="2" />
  </configuration>
</component>
----