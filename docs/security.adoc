= Security

== Identity

C'est la carte d'identité d'un utilisateur dans le sysstème.
Par exemple, lors de la création, on aura besoin de mettre son nom, prénom, email, adresse ....


== Authentication

Il faut se logger avec les identifiants (login et Mdp)

== Authorization

Une fois connecté, il faut une autorisation pour accéder à une ressource

* Superadmin :  il peut ajouter, supprimer ou modifier des ingrédients ou des recettes
* Admin : quelqu'un qui pourra créer des comptes ou en supprimer
* User : il peut se connecter, enregister des recettes en favori, il peut faire la même chose qu'un utilisateur anonyme
* Anonymous : chercher des recettes et/ou ingrédients

=== Resources

Parmi les ressources qu'on va manipuler :

* Les ingrédients
* Les recettes
* Les comptes utilisateurs

== Différentes possibilités :

* IdentityIQ
** Identity
** authentication
** authorization
** Connecteur : IIQ utilise un connecteur pour accéder aux différentes applications

* Oauth 2.0
** authorization

* OpenIdConnect (OIDC)
** Identification
** authentication
** authorization
*** il s'appuit sur Oauth 2.0

* Basic Access Auth
** authentication
*** Envoie de login et mot de passse dans les requêtes HTTP

* JWT
** authorization
** Identity

* Spring Security : Il se fait du côté de l'application en backend
** authentication
*** On peut utiliser du basic access auth pour indiquer comment est stocké le mot de passe
*** dans le cas où spring boot ne gère pas l'authentication, on peut demander à un serveur d'authorization à le faire pour nous (voir section authorization).
** Identity
*** On peut indiquer à spring de s'appuyer sur un annuaire (LDAP ou active Directory, IIQ) pour récupérer les identités
*** on peut aussi indiquer à spring d'utiliser notre base de données
** authorization
*** sprint peut donner accès à une ressource ( par exemple : une requête, ou une méthode de contrôleur)
*** On peut configurer spring security pour déléguer l'authorization à un serveur d'authorization( par exemple : Oauth2.0, OIDC)
*** Les informations d'authorization peuvent être portées par JWT ou encore dans une session


