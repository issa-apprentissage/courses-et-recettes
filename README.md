# Courses et recettes

- Parcours d'[apprentissage Java](docs/apprentissage-java.adoc)
  - [Documentation en ligne](https://issa-apprentissage.gitlab.io/courses-et-recettes/)

## Lancer le projet en local

Par défault ce projet se lance avec une base de données en mémoire.

```shell
mvn spring-boot:run
```

## Lancer le projet avec la base de données PostGres

Lancer la base de donnée

```shell
docker-compose -f docker-compose-postgres.yml up -d
```

Lancer l'application avec le profil spring `db` qui va utiliser la base de données qu'on vient de démarrer.

```shell
mvn spring-boot:run -Dspring-boot.run.profiles=db 
```

Quand l'application est démarrée, il s'est passé plusieurs choses

- Les tables de la base de données ont été créées automatiquement à partir des entités JPA.
- Des données initiales sont alimentées par le script `src/main/resources/data.sql`



## Lancer les tests unitaires 

```shell
mvn test
```

## Lancer les tests d'intégration 

```shell
mvn failsafe:integration-test
```

## Rechargement automatique 

Les outils de développement Spring sont actifs ainsi que le live reload.

Pour profiter du live reload dans le navigateur, installer une extension qui le supporte.

Par ex: https://chrome.google.com/webstore/detail/remotelivereload/jlppknnillhjgiengoigajegdpieppei